#define _CRT_SECURE_NO_WARNINGS 1
/*
小乐乐上课需要走n阶台阶，因为他腿比较长，
所以每次可以选择走一阶或者走两阶，
那么他一共有多少种走法？
分析：如果台阶数<=2,那就有n种走法，即走2次一阶或1次二阶
      如果走了1阶，还剩n-1阶，n-1阶又需要f(n-1)种走法
      如果走了2阶，还剩n-2阶，n-2阶又需要f(n-2)种走法
      因为不确定走的1还是2，所以两种情况加起来，即f(n-1)+f(n-2);
      所以f(n) = f(n-1)+f(n-2)
*/
//#include <stdio.h>
//int Fib(int n) {
//    if (n > 2)
//        return Fib(n - 1) + Fib(n - 2);
//    else
//        return n;
//}
//int main() {
//    int n = 0;
//    scanf("%d", &n);
//    int ret = Fib(n);
//    printf("%d\n", ret);
//    return 0;
//}

/*
* 变种水仙花
* 5位数中的所有 Lily Number，每两个数之间间隔一个空格
*/
//#include <stdio.h>
//int main() {
//    int i = 0;
//    for (i = 10000; i < 100000; i++) {
//        int sum = 0;
//        for (int j = 10; j <= 10000; j *= 10)
//            sum += i / j * (i % j);
//        if (sum == i)
//            printf("%d ", i);
//    }
//    return 0;
//}

#include <stdio.h>
/*
    三角形判断
*/
int main() {
    int a = 0, b = 0, c = 0;
    while (scanf("%d %d %d", &a, &b, &c) != EOF) {
        if (a + b > c && a + c > b && b + c > a) {
            if (a == b && b == c)
                printf("Equilateral triangle!\n");
            else if (a == b || b == c || a == c)
                printf("Isosceles triangle!\n");
            else
                printf("Ordinary triangle!\n");
        }
        else {
            printf("Not a triangle!\n");
        }
    }
    return 0;
}