#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
////使用指针打印数组内容
//int main() {
//	int arr[] = { 1,2,3,4,5,6 };
//	int* p = arr;
//	for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); i++) {
//		printf("%d ", *(p+i));
//	}
//	return 0;
//}


/*
* 打印菱形
分析：
	  *
	 ***
	*****
   *******
  *********
 ***********
*************
 ***********
  *********
   *******
	*****
	 ***
	  *
	  分为上下两部分打印
	  假设上面有n行，下面就有n-1行
	  每行由空格和*组成
	  上方区域：
			  每行的空格数 = 当前行 * 2 - 1，即 2 * i - 1
			  空格数 = 总行数 - 当前行，即 n - i
	  下方区域：
			  下方同理，只需要注意行数少1，倒序打印即可
*/
//#include<stdio.h>
//int main() {
//	//*************
//	int line = 0;
//	scanf("%d", &line);
//	for (int i = 1; i <= line; i++) {
//		for (int j = 1; j <= line - i; j++)
//			printf(" ");
//		for (int k = 1; k <= 2 * i - 1; k++) {
//			printf("*");
//		}
//		printf("\n");
//	}
//	for (int i = line - 1; i >= 1; i--) {
//		for (int j = 1; j <= line - i; j++)
//			printf(" ");
//		for (int k = 1; k <= 2 * i - 1; k++) {
//			printf("*");
//		}
//		printf("\n");
//	}
//	return 0;
//}

//打印“水仙花数”
//求出0～100000之间的所有“水仙花数”并输出
/*
	先求出有几位数，再累加每一位的n次方
*/
//#include<stdio.h>
//#include<math.h>
//
//int main() {
//	for (int i = 0; i <= 100000; i++) {
//		int sum = 0, n = 0,j = i,k = i;
//		while (j) {
//			n++;
//			j /= 10;
//		}
//		while (k) {
//			sum += pow(k % 10, n);
//			k /= 10;
//		}
//		if (sum == i)
//			printf("%d ",sum);
//	}
//	return 0;
//}

//求Sn=a+aa+aaa+aaaa+aaaaa的前5项之和
//#include<stdio.h>
//int main() {
//	int sum = 0;
//	int a = 0;
//	scanf("%d", &a);
//	int t = a;
//	for (int i = 0; i < 5; i++) {
//		sum += a;
//		a = a * 10 + t;
//	}
//	printf("%d\n", sum);
//	return 0;
//}

//输出逆序的字符串,数据范围[1,10000]
//#include<stdio.h>
//#include<string.h>
//void reverse(char* str) {
//	int l = 0, r = strlen(str) - 1;
//	while (l < r) {
//		char t = *(str + l);
//		 *(str + l) = *(str + r);
//		*(str + r) = t;
//		l++;
//		r--;
//	}
//}
//int main() {
//	char str[10001] = { 0 };
//	gets_s(str);
//	reverse(str);
//	puts(str);
//	return 0;
//}


//喝汽水，1瓶汽水1元，2个空瓶可以换一瓶汽水，给20元，可以多少汽水（编程实现）
#include<stdio.h>
int main() {
	int money = 0;
	int cnt = 0;
	int sum = 0;//得到的饮料总数
	scanf("%d", &money);
	sum = money;
	cnt = money;//空瓶子
	while (cnt >= 2) {
		sum++;
		cnt -= 2;
		cnt++;
	}
	printf("%d", sum);
	return 0;
}