#define _CRT_SECURE_NO_WARNINGS 1
//#include <stdio.h>
//交换两个变量（不创建临时变量）
//int main()
//{
//	//0^1=1,1^1=0
//	int a = 3, b = 5;
//	a = a ^ b; //3 ^ 5
//	b = a ^ b; //3 ^ 5 ^ 5
//	a = a ^ b; //3 ^ 5 ^ 5 ^ 3 ^ 5
//	printf("%d %d\n", a, b);
//	return 0;
//}


//统计二进制中1的个数
//#include<stdio.h>
// 方法一
//int NumberOf1(int n) {
    /*
        规律: n & (n-1) 可以把n的最低位的1变成0;
        思路: 只要n不等于0,就继续 n & (n-1)操作,每次操作找到一个1
                即有几个1就进行几次
        12      1100
        11      1011
        -------------
        8       1000
    */
//    int cnt = 0;
//    while (n) {
//        cnt++;
//        n &= n - 1;
//    }
//    return cnt;
//}
// 
// 方法二
// 将复数转变成无符号数,每次获取最低位，然后右移
// int NumberOf1(unsigned int n ) {
//int cnt = 0;
//while (n) {
//    if ((n & 1) == 1)
//        cnt++;
//    n = n >> 1;
//}
//return cnt;
//}
//int main() {
//    int n = 0;
//    scanf("%d", &n);
//    int ret=NumberOf1(n);
//    printf("%d\n", ret);
//	return 0;
//}

//打印整数二进制的奇数位和偶数位
//#include<stdio.h>
//void print(int n) {
//    //假设从0开始算
//    //打印偶数位
//    for (int i = 30; i >= 0; i -= 2) {
//        printf("%d", (n >> i) & 1);
//    }
//    printf("\n");
//    //奇数位
//    for (int i = 31; i >= 1; i -= 2) {
//        printf("%d", (n >> i) & 1);
//    }
//}
//int main() {
//    int n = 0;
//    scanf("%d", &n);
//    print(n);
//    return 0;
//}

//#include<stdio.h>
////求两个数二进制中不同位的个数
////挨个比较
//int count(int m, int n) {
//    int cnt = 0;
//    for (int i = 0; i < 32; i++) {
//        if (((m>>i) & 1) != ((n>>i) & 1))
//            cnt++;
//    }
//    return cnt;
//}
////异或后，判断有几个1
//int count2(int m, int n) {
//    int s = m ^ n;
//    int cnt = 0;
//    while (s) {
//        cnt++;
//        s &= s - 1;
//    }
//    return cnt;
//}
//int main() {
//    int m = 0, n = 0;
//    scanf("%d %d", &m, &n);
//    int ret=count2(m, n);
//    printf("%d\n", ret);
//    return 0;
//}
