#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#define Max 100
int main()
{
	//打印前n行的杨辉三角
	int n = 0;
	scanf("%d", &n);
	int arr[Max][Max] = {0};
	//i控制行数
	for (int i = 0; i < n; i++) {
		//给每一行的首末元素赋值
		arr[i][0] = arr[i][i] = 1;
		//j控制列数，给每一行的非首末元素赋值
		for (int j = 1; j < i; j++) {
			arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
		}
	}
	//输出杨辉三角
	for (int i = 0; i < n; i++) {
		//控制打印格式
		for(int k = 0; k < n-i-1; k++) {
			printf("    ");
		}
		for (int j = 0; j <= i; j++) {
			printf("%-4d", arr[i][j]);
			printf("    ");
		}
		//每输出一行就换行
		printf("\n");
	}
	return 0;
}
