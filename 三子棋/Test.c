#define _CRT_SECURE_NO_WARNINGS 1
//自己的头文件用双引号引起来
#include"game.h"

void menu() {
	printf("**********************\n");
	printf("******  1.play  ******\n");
	printf("******  0.exit  ******\n");
	printf("**********************\n");
}
void game() {
	int ret = 0;
	//创建棋盘
	char board[ROW][COL] = {0};
	//初始化棋盘
	InitBoard(board,ROW,COL);
	//显示棋盘
	DisplayBoard(board, ROW, COL);
	while (1) {
		//玩家下
		PlayerMove(board, ROW, COL);
		//判胜负
		ret = IsWin(board, ROW, COL);
		if(ret != 'C')
			break;
		//电脑下
		ComputerMove(board, ROW, COL);
		//判胜负
		ret=IsWin(board, ROW, COL);
		if ('C' != ret)
			break;
	}
	if ('*' == ret)
		printf("玩家赢!\n");
	else if ('D' == ret)
		printf("平局!\n");
	else
		printf("电脑赢!\n");
	Sleep(5000);
	system("cls");
}
int main() {
	srand((unsigned int)time(NULL));
	int input = 0;
	do {
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		switch (input) {
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		defaule:
			printf("输入错误,请重新输入！\n");
			break;
		}
	} while (input);
	return 0;
}