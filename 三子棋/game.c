#define _CRT_SECURE_NO_WARNINGS 1
#include "game.h"

void InitBoard(char board[ROW][COL], int row, int col) {
	for (int i = 0; i < row; i++) {
		for (int j = 0; j < col; j++) {
			board[i][j] = ' ';
		}
	}
}

/*
* 一行数据，一行边框
   |   |   
---|---|---
   |   |   
---|---|---
   |   |   
 */ 

void DisplayBoard(char board[ROW][COL], int row, int col) {
	for (int i = 0; i < row; i++) {
		//打印数据行
		for (int j = 0; j < col; j++) {
			if (j < col - 1)
				printf(" %c |", board[i][j]);
			else
				printf(" %c \n", board[i][j]);
		}
		//打印边框行
		if (i < row - 1) {
			for (int j = 0; j < col; j++) {
				if (j < col - 1)
					printf("---|");
				else
					printf("---\n");
			}
		}
	}
}

int IsFull(char board[ROW][COL], int row, int col) {
	for (int i = 0; i < row; i++) {
		for (int j = 0; j < col; j++) {
			if (' ' == board[i][j]) {
				return 0;
			}
		}
	}
	return 1;
}
//#:电脑
//*:玩家
void PlayerMove(char board[ROW][COL], int row, int col) {
	int x=0, y=0;
	printf("玩家走:>\n");
	while (1) {
		printf("请输入要下的坐标:>");
		scanf("%d %d", &x, &y);
		if (x<1 || x>row || y<1 || y>col) {
			printf("该坐标不合法,请重新输入!\n");
			continue;
		}
		if (' ' == board[x - 1][y - 1]) {
			board[x - 1][y - 1] = '*';
			break;
		}
		else
			printf("该位置被占用，请重新输入!\n");
	}
	DisplayBoard(board, ROW, COL);
}

void ComputerMove(char board[ROW][COL], int row, int col) {
	int x = 0; 
	int y = 0; 
	printf("电脑走:>\n");
	while (1) {
		x = rand() % row;
		y = rand() % row;
		if (board[x][y] == ' ') {
			board[x][y] = '#';
			break;
		}
	}
	DisplayBoard(board,ROW,COL);
}
/*
*:玩家赢
#:电脑赢
D;平局
C:继续
*/
char IsWin(char board[ROW][COL], int row, int col) {
	//判断行
	for (int i = 0; i < row; i++) {
		if (board[i][0] == board[i][1] && board[i][0]==board[i][2] && board[i][0] != ' ')
			return board[i][0];
	}
	//判断列
	for (int j = 0; j < col; j++) {
		if (board[0][j] == board[1][j] && board[0][j]==board[2][j] && board[0][j] != ' ')
			return board[0][j];
	}
	//判断对角线
	if (board[0][0] == board[1][1] && board[1][1]==board[2][2] && board[1][1] != ' ')
		return board[1][1];
	if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[1][1] != ' ')
		return board[1][1];
	//判断是否下满(平局)
	if (IsFull(board,ROW,COL))
		return 'D';
	return 'C';
}