#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
//#include<math.h>
//int main() {
//	//求素数(普通方法)
//	int cnt = 0;
//	int i = 0;
//	int j = 0;
//	for (i = 100; i <= 200; i++) {
//		for (j = 2; j < i; j++) {
//			if (i % j == 0) {
//				break;
//			}
//		}
//		if (i == j) {
//			printf("%d ", i);
//			cnt++;
//		}
//	}
//	printf("\ncnt=%d", cnt);
//	return 0;
//}

//#include<stdio.h>
//#include<math.h>
//int main() {
//	//改进后
//	int i = 0;
//	int j = 0;
//	int cnt = 0;
//	for (i = 101; i <= 200; i+=2) {//去掉偶数
//		int flag = 1;//假设i是素数
//		for (j = 2; j <= sqrt(i); j++) {
//			if (i%j==0) {
//				flag = 0;
//				break;//不是素数就跳出循环
//			}
//		}
//		if (flag) {
//			printf("%d ", i);
//			cnt++;
//		}
//	}
//	printf("\ncnt=%d", cnt);
//	return 0;
//}

//#include<stdio.h>
//int main() {
//	//求1到100的整数有多少个9
//	int cnt = 0;
//	int i = 0;
//	for (i = 1; i <= 100; i++) {
//		int t = i;
//		while (t) {
//			if (t % 10 == 9)//每次获取个位数和9比较
//				cnt++;
//			t /= 10;//每次小十倍
//		}
//	}
//	printf("cnt=%d\n", cnt);
//	return 0;
//}

////辗转相除法
//#include<stdio.h>
//int main() {
//	int m = 0, n = 0;
//	int k = 0;//余数
//	scanf("%d %d", &m, &n);
//	while (k=m % n) {
//		m = n;//始终作为被除数
//		n = k;//始终作为除数
//	}
//	printf("%d", n);
//	return 0;
//}

//更相减损术
//#include<stdio.h>
//int main() {
//	int m = 0, n = 0;
//	scanf("%d %d", &m, &n);
//	while (m != n) {
//		if (m > n)
//			m -= n;
//		else
//			n -= m;
//	}
//	printf("%d", n);
//	return 0;
//}

//#include<stdio.h>
//int is_leap_year(int year)
//{
//	if ((year % 400 == 0) || (year % 4 == 0 && year % 100 != 0))
//		return 1;
//	else
//		return 0;
//}

//int main()
//{
//	int y = 0;
//	for (y = 1000; y <= 2000; y++)
//	{
//		//判断y是否为闰年
//		//如果是闰年返回1
//		//不是闰年返回0
//		if (is_leap_year(y))
//		{
//			printf("%d ", y);
//		}
//	}
//	return 0;
//}

////猜数字小游戏
//#include<stdio.h>
//#include<stdlib.h>
//#include<time.h>
//
//void menu() {
//	printf("*******************\n");
//	printf("*****1.玩游戏******\n");
//	printf("*****0.退  出******\n");
//	printf("*******************\n");
//}
//void game() {
//	int input = 0;
//	//生成随机数[0,99] -> [1,100]
//	int ret = rand()%100+1;
//	//printf("%d\n", ret);
//	printf("请输入:");
//	while (1) {
//		scanf("%d", &input);
//		if (input > ret)
//			printf("猜大了,请继续：");
//		else if (input < ret)
//			printf("猜小了，请继续：");
//		else {
//			printf("恭喜你，猜对了!\n");
//			Sleep(4000);
//			system("cls");
//			break;
//		}
//	}
//}
//int main() {
//	//设置随机数的生成器(只需设置一次)
//	srand((unsigned int)time(NULL));
//	int input = 0;
//	do {
//		menu();
//		printf("请选择:");
//		scanf("%d", &input);
//		switch (input) {
//		case 1:
//			game();
//			break;
//		case 0:
//			exit(-1);
//		default:
//			printf("输入错误！\n");
//			break;
//		}
//	} while (1);
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	//二分查找
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int left = 0;//左下标
//	int right = sz-1;//右下标
//	int mid = 0;//中间元素下标
//	int val = 0;//待查找的值
//	int flag = 0;//判断是否查找成功
//	scanf("%d", &val);
//	while (left <= right) {
//		//mid = (left + right) / 2;
//		mid = left + (right - left) / 2;
//		if (val > arr[mid]) {
//			left = mid + 1;
//		}
//		else if (val < arr[mid]) {
//			right = mid - 1;
//		}
//		else{
//			flag = 1;
//			break;
//		}
//	}
//	if (flag) {
//		printf("找到了，下标为%d\n", mid);
//	}
//	else {
//		printf("没找到");
//	}
//	return 0;
//}

#include<stdio.h>
int main() {
	int n = 0;
	int k = 0;
	scanf("%d %d", &n, &k);
	int arr[100] = {0};
	int l = 0, r = 0;
	int mid = 0;
	for (int i = 0; i < n; i++) {
		scanf("%d",&arr[i]);
	}
	r = arr[0];
	for (int j = 1; j < n; j++) {
		if (r < arr[j])
			r = arr[j];
	}
	int t = 0;
	while (l <= r) {
		int sum = 0;
		mid = l + (r - l) / 2;
		for (int i = 0; i < n; i++) {
			sum += (arr[i] / mid);
		}
		if (sum > k) {
			l = mid;
		}
		else if (sum < k) {
			r = mid;
		}
		else {
			break;
		}
	}
	printf("%d\n", mid);
	return 0;
}