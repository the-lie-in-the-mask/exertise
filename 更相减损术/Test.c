#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main() {
	//更相减损术
	int a = 0, b = 0;
	int min = 0;//减数
	int t = 0;//余数(被减数)
	scanf("%d %d", &a, &b);
	//如果a和b相等，最大公约数就是他们本身
	if (a == b) {
		t = a;
	}
	//不相等的话，开始找较小值
	else {
		if (a > b) {
			min = b;
			t = a - min;
		}
		else {
			min = a;
			t = b - min;
		}
		//余数和最小值不断相减，当相等时停止
		while (t != min) {
			if (t > min) {
				t -= min;
			}
			else {
				int v = t;
				t = min - t;//t始终为余数(被减数)
				min = v;//min始终作为减数
			}
		}
	}
	printf("最大公约数:%d\n", t);
	return 0;
}