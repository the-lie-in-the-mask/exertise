#define _CRT_SECURE_NO_WARNINGS 1

//斐波那契数
//#include<stdio.h>
//递归算法
//int Fib1(int n) {
//	if (n <= 2)
//		return 1;
//	else
//		return Fib1(n - 1) + Fib1(n - 2);
//}
//
//迭代算法
//int Fib2(int n) {
//	int a = 1;
//	int b = 1;
//	int c = 1;
//	while (n >= 3) {
//		c = a + b;
//		a = b;//始终为第n-1个数
//		b = c;//始终为第n-2个数
//		n--;
//	}
//	return c;
//}
//int main() {
//	int n = 0;
//	scanf("%d", &n);
//	int ret = Fib1(n);
//	printf("%d\n", ret);
//
//	scanf("%d", &n);
//	ret = Fib2(n);
//	printf("%d\n", ret);
//	return 0;
//}

//n的K次方
//#include<stdio.h>
//double Pow(double n,int k) {
//	if (k == 0)
//		return 1;
//	else if (k == 1)
//		return n;
//	else
//		return n * Pow(n, k - 1);
//}
//int main() {
//	double n = 0;
//	int k = 0;
//	scanf("%lf", &n);
//	scanf("%d", &k);
//	double ret=Pow(n,k);
//	printf("%.2lf\n", ret);
//	return 0;
//}

//计算一个数的每位之和
//#include<stdio.h>
//int DigitSum(int n) {
//	if (n < 10) //终止的条件(n为个位数)
//		return n;
//	else
//		//每次获取个位数，减小十倍
//		return n % 10 + DigitSum(n/10);
//}
//int main() {
//	int n = 0;
//	scanf("%d", &n);
//	int ret = DigitSum(n);
//	printf("%d", ret);
//	return 0;
//}

//字符串逆序
//#include<stdio.h>
//非递归
//void reverse_string(char* arr) {
//	int left = 0;
//	int right = strlen(arr)-1;
//	while (left < right) {
//		char t = arr[left];//t=*(arr+left);
//		arr[left] = arr[right];//*(arr+left)=*(arr+right)
//		arr[right] = t;//*(arr+right)=t
//		left++;
//		right--;
//	}
//}
//递归
//void reverse_string(char* arr) {
//	char t = *arr;//t=a
//	int len = strlen(arr);
//	*arr = *(arr + len - 1);//a=b
//	*(arr + len - 1) = '\0';//末位置置为'\0'
//	if (strlen(arr+1) >= 2) {//一次交换后，判断(a,b）是否可以继续交换
//		reverse_string(arr+1);
//	}
//	*(arr + len - 1) = t;//
//}
//int main() {
//	char arr[] = "abcdef";
//	reverse_string(arr);
//	printf("%s\n", arr);
//	return 0;
//}

//模拟strlen
//#include<stdio.h>
//递归算法
//int my_strlen1(char* arr) {
//	if (*arr == '\0')
//		return 0;
//	else
//		return 1 + my_strlen1(arr + 1);
//}
//非递归算法
//int my_strlen2(char* arr) {
//	int cnt = 0;
//	while (*(arr++) != '\0') {
//		cnt++;
//	}
//	return cnt;
//}
//int main() {
//	char arr[]="qwerdf";
//	int ret=my_strlen1(arr);
//	printf("%d\n",ret);
//	ret = my_strlen2(arr);
//	printf("%d\n", ret);
//	return 0;
//}


//#include<stdio.h>
////求阶乘
////递归
//int Fac1(int n) {
//	if (n <= 1) {
//		return 1;
//	}
//	else {
//		return n * Fac1(n - 1);
//	}
//}
////非递归
//int Fac2(int n) {
//	int t = 1;
//	for (int i = 2; i <=n; i++) {
//		t *= i;
//	}
//	return t;
//}
//int main() {
//	int n= 0;
//	scanf("%d", &n);
//	int ret1 = Fac1(n);
//	int ret2 = Fac2(n);
//	printf("%d\n", ret1);
//	printf("%d\n", ret2);
//	return 0;
//}

//#include<stdio.h>
////递归
//void print(int n) {
//	if (n) {
//		print(n/10);
//		printf("%d ", n%10);
//	}
//}
//int main() {
//	int n = 0;
//	scanf("%d", &n);
//	print(n);
//	return 0;
//}