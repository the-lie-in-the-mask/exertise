#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#define MAXSIZE 100	//为顺序表分配空间的容量
typedef int ElemType;
typedef struct {
	ElemType* elem;//顺序表的基地址
	int length;//顺序表的当前长度
	int listsize;//顺序表的当前容量
}SqList;

//初始化操作
void InitList(SqList& L) {
	L.elem = (ElemType*)malloc(sizeof(SqList) * MAXSIZE);
	if (!L.elem) {
		printf("分配空间失败!\n");
		exit(-1);
	}
	L.length = 0;
	L.listsize = MAXSIZE;
}
//判空操作
int ListEmpty(SqList L){
	if (L.length == 0) {
		return 1;
	}
	else {
		return 0;
	}
}
//求顺序表的长度
int ListLength(SqList L) {
	return L.length;
}
//插入操作(在第i个元素之前插入值为x的元素)
void ListInsert(SqList& L, ElemType x, int i) {
	//判断插入位置是否合法(1<=i<=L.length+1)
	if (i<1 || i>L.length+1) {
		printf("插入位置不合法!\n");
		return;
	}
	if (L.length >= L.listsize) {
		ElemType* newbase = (ElemType*)realloc(L.elem, sizeof(SqList) * (L.listsize*2));
		if (!newbase) {
			printf("分配空间失败!\n");
			exit(-1);
		}
		L.elem = newbase;
		L.listsize *= 2;
	}
	ElemType* p = &(L.elem[i-1]);//指向待插入元素位置
	ElemType* q;//始终指向待移动的元素位置
	for(q = &(L.elem[L.length - 1]);q>=p;q--){
		*(q+1) = *q;
	}
	*p = x;
	L.length++;
}
//删除操作(删除第i个元素，并用e返回其值)
void ListDelete(SqList& L, int i, ElemType& e) {
	if (i<1 || i>L.length) {
		printf("删除位置不合法!\n");
		return;
	}
	ElemType* p = &(L.elem[i - 1]);//先保存待删元素位置
	e = *p;
	ElemType* q=&(L.elem[L.length-1]);//指向最后一个元素
	for (++p; p <= q; p++) {	//p始终指向待移动元素
		*(p-1) = *p;
	}
	L.length--;
}
//查找操作(查找首个值为e的元素,并返回它的位序号)
int LocateElem(SqList L, ElemType e) {
	int i;
	for (i = 1; i <= L.length && L.elem[i - 1] != e; i++) {
		;
	}
	if (i <= L.length) {
		return i;
	}
	else {
		return 0;
	}
}
//输出操作
void DisplayList(SqList L) {
	for (int i = 0; i < L.length; i++) {
		printf("%d  ", L.elem[i]);
	}
	printf("\n");
}
//逆置操作
void ReverseSqList(SqList& L) {
	for (int i = 0; i < L.length / 2; i++) {
		ElemType t = L.elem[i];
		L.elem[i] = L.elem[L.length - 1 - i];
		L.elem[L.length - 1-i] = t;
	}
}
//***********************************************

//删除有序顺序表中所有的重复元素
void DeDuplicates(SqList& L) {	
	for (int i = 0; i < L.length - 1; ) {
		if (L.elem[i]==L.elem[i+1]) {
			for (int p = i + 1; p < L.length; p++) {
				L.elem[p - 1] = L.elem[p];
			}
			L.length--;
		}
		else {
			i++;
		}
	}
}
//在有序顺序表中插入一个元素仍保持有序(从小到大)
void ListInsert_SOT(SqList& L,int x) {
	int i;
	if (L.length >= L.listsize) {
		ElemType* newbase = (ElemType*)realloc(L.elem,sizeof(SqList)*(L.listsize*2));
		if (!newbase) {
			printf("分配空间失败!\n");
			exit(-1);
		}
		L.elem = newbase;
		L.listsize *= 2;
	}
	for (i = L.length-1; i>=0&&x<L.elem[i]; i--) {
		L.elem[i+1] = L.elem[i];//边比较边后移
	}
	L.elem[i+1] = x;
	L.length++;
}
//合并操作(将两个非递减有序表合并为一个新的有序表)
void MergeList(SqList La,SqList Lb,SqList& Lc) {
	//先将La中的元素全放到Lc中
	for (int i = 0; i<La.length; i++) {
		ListInsert(Lc,La.elem[i],i+1);
	}
	for (int j = 0; j < Lb.length; j++) {
		ListInsert_SOT(Lc,Lb.elem[j]);
	}
}
int main()
{
	ElemType x,e;
	int i=0;
	int m = 0, n = 0;
	SqList L;
	InitList(L);
	printf("请输入顺序表中各元素的值:\n");
	for (int j = 0; j < 6;j++) {
		scanf("%d", &x);
		ListInsert(L, x, j+1);
	}
	printf("该顺序表的长度为:%d\n", ListLength(L));
	DisplayList(L);
	printf("请分别输入要插入元素的值和位序号:\n");
	scanf("%d %d", &x,&i);
	ListInsert(L,x,i);
	DisplayList(L);
	printf("请输入您要删除元素的位序号:\n");
	scanf("%d", &i);
	ListDelete(L,i,e);
	printf("删除后:\n");
	DisplayList(L);
	printf("被删除的元素为:%d\n", e);
	printf("请输入要查找的元素:\n");
	scanf("%d", &e);
	printf("%d的位序号为:%d\n", e, LocateElem(L,e));
	printf("*********************\n");

	
	SqList L1;
	InitList(L1);
	printf("请输入有序顺序表中各元素的值:\n");
	for (int j = 0; j < 6; j++) {
		scanf("%d", &x);
		ListInsert(L1, x, j + 1);
	}
	DeDuplicates(L1);
	DisplayList(L1);
	printf("请输入要插入的元素的值:");
	scanf("%d",&x);
	ListInsert_SOT(L1, x);
	DisplayList(L1);
	ReverseSqList(L1);
	printf("逆置后:\n");
	DisplayList(L1);
	printf("************************\n");
	
	SqList La, Lb, Lc;
	InitList(La);
	InitList(Lb);
	InitList(Lc);
	printf("请输入La的各元素值:");
	for (int i = 0; i < 4; i++) {
		scanf("%d", &x);
		ListInsert(La, x,i+1);
	}
	printf("请输入Lb的各元素值:");
	for (int i = 0; i < 6; i++) {
		scanf("%d", &x);
		ListInsert(Lb, x, i + 1);
	}
	MergeList(La, Lb, Lc);
	printf("合并后:");
	DisplayList(Lc);
	return 0;
}